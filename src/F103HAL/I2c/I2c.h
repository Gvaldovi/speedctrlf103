/****************************************************************************************************/
/**
  \file         I2c.h
  \brief        
  \author       Gerardo Valdovinos
  \project      IKMaster
  \version      
  \date         Aug 16, 2016

  ESTE DOCUMENTO Y SU CONTENIDO ES PROPIEDAD DE SIREMCO S.A.P.I. DE C.V. LAS COPIAS IMPRESAS DE ESTE
  DOCUMENTO SON COPIAS NO CONTROLADAS. PROHIBIDA SU REPRODUCCION PARCIAL O TOTAL SIN CONSENTIMIENTO
  DE SIREMCO S.A.P.I. DE C.V. SIREMCO(R) - CONFIDENCIAL -
*/
/****************************************************************************************************/
#ifndef __I2C_H
#define	__I2C_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "I2cCfg.h"
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/
#define I2C_ON								0
#define I2C_BUSY							1
#define I2C_NO_ACK							2
/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/
#define I2C_ADDRESS_BASE					0x40005400
#define I2C_ADDRESS_OFFSET					0x400

#define I2C_CR1(c)							*(u32 volatile*)(I2C_ADDRESS_BASE + (I2C_ADDRESS_OFFSET * c) + 0x00)
#define I2C_CR1_SET(c, x)					I2C_CR1(c) |= (u32)(x)
#define I2C_CR1_CLEAR(c, x)					I2C_CR1(c) &= (u32)~(x)

#define I2C_CR2(c)							*(u32 volatile*)(I2C_ADDRESS_BASE + (I2C_ADDRESS_OFFSET * c) + 0x04)
#define I2C_CR2_SET(c, x)					I2C_CR2(c) |= (u32)(x)

#define I2C_DR(c)							*(u32 volatile*)(I2C_ADDRESS_BASE + (I2C_ADDRESS_OFFSET * c) + 0x10)
#define I2C_DR_READ(c)						(u8)I2C_DR(c)
#define I2C_DR_WRITE(c, x)                  I2C_DR(c) = x

#define I2C_SR1(c)							*(u32 volatile*)(I2C_ADDRESS_BASE + (I2C_ADDRESS_OFFSET * c) + 0x14)
#define I2C_SR1_SET(c, x)					I2C_SR1(c) |= (u32)(x)
#define I2C_SR1_GET(c, x)                   ((I2C_SR1(c) & x) == x)
#define I2C_SR1_CLEAR(c, x)					I2C_SR1(c) &= (u32)~(x)

#define I2C_SR2(c)							*(u32 volatile*)(I2C_ADDRESS_BASE + (I2C_ADDRESS_OFFSET * c) + 0x18)
#define I2C_SR2_READ(c)						(u8)I2C_SR2(c)

#define I2C_CCR(c)							*(u32 volatile*)(I2C_ADDRESS_BASE + (I2C_ADDRESS_OFFSET * c) + 0x1C)
#define I2C_CCR_SET(c, x)					I2C_CCR(c) |= (u32)(x)

#define I2C_TRISE(c)						*(u32 volatile*)(I2C_ADDRESS_BASE + (I2C_ADDRESS_OFFSET * c) + 0x20)
#define I2C_TRISE_SET(c, x)					I2C_TRISE(c) |= (u32)(x)

#define I2C_APB1ENR(x)						RCC->APB1ENR |= (1 << (21 + x))
/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef struct
{
	teI2cChannel				eChannel;				/* I2C channel */
	u8							u8Status;				/* Status of driver */
	u8							u8Address;				/* Address */
	u16                         u16Size;            	/* Size */
	u8*                         pu8Data;            	/* Pointer to data */
}tsI2cChannel;

/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
void vfnI2cInit(const tsI2cDriverCfg *psDriverCfg);
u8 u8fnI2cIsBusy(teI2cChannel Channel);

void vfnI2cEnable(teI2cChannel Channel);
void vfnI2cDisable(teI2cChannel Channel);
//u8 u8fnI2cGetStatus(teI2cChannel Channel);
u8 u8fnI2cRead(teI2cChannel Channel, u8 u8Address, u8 *pu8Data, u16 u16Size);
u8 u8fnI2cWrite(teI2cChannel Channel, u8 u8Address, u8 *pu8Data, u16 u16Size);
#endif	/* __I2C_H */
/***************************************End of File**************************************************/
