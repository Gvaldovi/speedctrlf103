/****************************************************************************************************/
/**
  \file         iWatchdog.h
  \brief        Independent watchdog
  \author       Gerardo Valdovinos
  \project      IKMaster
  \version      0.0.1
  \date         Dec 10, 2017

  ESTE DOCUMENTO Y SU CONTENIDO ES PROPIEDAD DE SIREMCO S.A.P.I. DE C.V. LAS COPIAS IMPRESAS DE ESTE
  DOCUMENTO SON COPIAS NO CONTROLADAS. PROHIBIDA SU REPRODUCCION PARCIAL O TOTAL SIN CONSENTIMIENTO
  DE SIREMCO S.A.P.I. DE C.V. SIREMCO(R) - CONFIDENCIAL -
*/
/****************************************************************************************************/
#ifndef __I_WATCHDOG_H
#define	__I_WATCHDOG_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "Typedefs.h"

/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/
#define WD_MODIFY           0x5555u
#define WD_START            0xCCCCu
#define WD_RELOAD           0xAAAAu
/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
/* This enum items have an average of time because the variation of LSI clock */
typedef enum
{
    WD_500_MS = 0,
    WD_1_S,
    WD_2_S,
    WD_4_S,
    WD_8_S,
    WD_16_S,
    WD_32_S,
}tWatchdogPrs;
/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
void vfniWatchdogInit(void);
void vfniWatchdogStart(void);
void vfniWatchdogReload(void);

#endif	/* __I_WATCHDOG_H */
/***************************************End of File**************************************************/
