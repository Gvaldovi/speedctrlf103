/****************************************************************************************************/
/**
  \file         SpiCfg.h
  \brief
  \author       Gerardo Valdovinos
  \project      IKMaster
  \version
  \date         Mar 21, 2017

  ESTE DOCUMENTO Y SU CONTENIDO ES PROPIEDAD DE SIREMCO S.A.P.I. DE C.V. LAS COPIAS IMPRESAS DE ESTE
  DOCUMENTO SON COPIAS NO CONTROLADAS. PROHIBIDA SU REPRODUCCION PARCIAL O TOTAL SIN CONSENTIMIENTO
  DE SIREMCO S.A.P.I. DE C.V. SIREMCO(R) - CONFIDENCIAL -
*/
/****************************************************************************************************/

#ifndef __SPI_CFG_H_
#define __SPI_CFG_H_

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "Typedefs.h"
#include "Gpio.h"
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/
/* Baud rates based on a 84MHz frequency (APB2). */
typedef enum
{
	eSPI_21_MHz = 1,	// 84MHz / 4
	eSPI_10500_KHZ,		// 84MHz / 8
	eSPI_5250_KHZ,		// 84MHz / 16
	eSPI_2625_KHZ,		// 84MHz / 32
	eSPI_1312500_HZ,	// 84MHz / 64
	eSPI_656250_HZ		// 84MHz / 128
}teSpiBaud;

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef enum
{
	eSPI_CH1 = 0,
//	eSPI_CH2,
//	eSPI_CH3,
	eSPI_CH_MAX
}teSpiChannel;

typedef struct
{
	teSpiChannel				eChannel;				/* SPI channel */
	teSpiBaud					eBaud;					/* Baud Rate */
	IRQn_Type           		tIRQ;         			/* Interrupt request routine */

	tsGpioChannelCfg			sMOSI;					/* Gpio MOSI configuration */
	tsGpioChannelCfg			sMISO;					/* Gpio MISO configuration */
	tsGpioChannelCfg			sSCK;					/* Gpio SCK configuration */
}tsSpiChannelCfg;

typedef struct
{
	u8                          u8Channels; 		/* Number of channels */
	const tsSpiChannelCfg      *psChannelCfg;  		/* Channels configuration pointer */
}tsSpiDriverCfg;
/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
extern const tsSpiDriverCfg sSpiDriverCfg;

#endif /* __SPI_CFG_H_ */
/***************************************End of File**************************************************/



