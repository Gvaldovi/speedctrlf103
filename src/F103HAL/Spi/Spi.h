/**
  \file         Spi.h
  \brief
  \author       Gerardo Valdovinos
  \project      IKMaster
  \version
  \date         Mar 21, 2017

  ESTE DOCUMENTO Y SU CONTENIDO ES PROPIEDAD DE SIREMCO S.A.P.I. DE C.V. LAS COPIAS IMPRESAS DE ESTE
  DOCUMENTO SON COPIAS NO CONTROLADAS. PROHIBIDA SU REPRODUCCION PARCIAL O TOTAL SIN CONSENTIMIENTO
  DE SIREMCO S.A.P.I. DE C.V. SIREMCO(R) - CONFIDENCIAL -
*/
/****************************************************************************************************/
#ifndef __SPI_H
#define	__SPI_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "SpiCfg.h"
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/
#define SPI_ON								0
#define SPI_BUSY							1
#define SPI_NO_ACK							2
/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

#define SPI_CR1(ch)							*(u32*)(au32SpiAddress[ch] + 0x00)
#define SPI_CR1_SET(ch, x)					SPI_CR1(ch) |= (u16)(x)
#define SPI_CR1_CLEAR(ch, x)				SPI_CR1(ch) &= (u32)~(x)
#define SPI_CR1_WRITE(ch, x)				SPI_CR1(ch) = (u32)(x)
#define SPI_BR_WRITE(ch, y)					SPI_CR1(ch) |= (y << 3)

#define SPI_CR2(ch)							*(u32*)(au32SpiAddress[ch] + 0x04)
#define SPI_CR2_SET(ch, x)					SPI_CR2(ch) |= (u32)(x)
#define SPI_CR2_CLEAR(ch, x)				SPI_CR2(ch) &= (u32)~(x)
#define SPI_CR2_WRITE(ch, x)				SPI_CR2(ch) = (u32)(x)

#define SPI_SR(ch)							*(u32*)(au32SpiAddress[ch] + 0x08)
#define SPI_SR_SET(ch, x)					SPI_SR(ch) |= (u32)(x)
#define SPI_SR_GET(ch, x)                   ((SPI_SR(ch) & x) == x)
#define SPI_SR_CLEAR(ch, x)					SPI_SR(ch) &= (u32)~(x)

#define SPI_DR(ch)							*(u32*)(au32SpiAddress[ch] + 0x0C)
#define SPI_DR_READ(ch)						(u8)SPI_DR(ch)
#define SPI_DR_WRITE(ch, x)                  SPI_DR(ch) = x

#define SPI_APB1ENR(ch)						RCC->APB1ENR |= (1 << (13 + ch))
#define SPI_APB2ENR(ch)						RCC->APB2ENR |= (1 << (12 + ch))
/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef struct
{
	teSpiChannel				eChannel;				/* SPI channel */
	u8							u8Status;				/* Status of driver */
	u16                         u16Size;            	/* Size */
	u8*                         pu8Data;            	/* Pointer to data */
	vpfn                        pfnTxCallback;			/* Transmission callback */
}tsSpiChannel;

/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
void vfnSpiInit(const tsSpiDriverCfg *psDriverCfg);
void vfnSpiDeInit(teSpiChannel Channel);

u8 u8fnSpiIsBusy(teSpiChannel Channel);
void vfnSpiEnable(teSpiChannel Channel);
void vfnSpiDisable(teSpiChannel Channel);
void vfnSpiSetCallback(teSpiChannel Channel, vpfn pfnCallback);
u8 u8fnSpiSend(teSpiChannel Channel, u16 u16Size, u8 *pu8Data);

#endif	/* __SPI_H */

/***************************************End of File**************************************************/

