/****************************************************************************************************/
/**
  \file         Gpio.c
  \brief        
  \author       Gerardo Valdovinos
  \project      Bluepill
  \version
  \date         Mar 15, 2019

  THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
#include "Gpio.h"
/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/

/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/

/*****************************************************************************************************
*                                        Variables EXTERN                                            *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Configuration of a I/O pin. Speed is 50Mhz as default.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnGpioInit(const tsGpioDriverCfg *DriverCfg)
{
	u8 i;

	if(DriverCfg->u8Channels)
	{
		for(i = 0;i < DriverCfg->u8Channels;i++)
		{
			vfnGpioCfg((tsGpioChannelCfg*)&DriverCfg->psChannelCfg[i]);
		}
	}
}
#define GPIO_CONF_SHIFT		2
#define GPIO_MODE_SHIFT		0
/*****************************************************************************************************
* \brief    Configuration of a I/O pin
* \author	Gerardo Valdovinos   
* \param    void
* \return   void
*****************************************************************************************************/
void vfnGpioCfg(tsGpioChannelCfg *psGpioCfg)
{
	u8 u8Cnf;

	/* Turn on clock of GPIO */
	GPIO_AHB1ENR(psGpioCfg->ePort);

	/* Select the correct configuration */
	switch(psGpioCfg->eCfg)
	{
	case eGPIO_OUT_PP :
		u8Cnf = (0 << GPIO_CONF_SHIFT) | (1 << GPIO_MODE_SHIFT);
		break;
	case eGPIO_OUT_OD:
		u8Cnf = (1 << GPIO_CONF_SHIFT) | (1 << GPIO_MODE_SHIFT);
		break;
	case eGPIO_AF_PP:
		u8Cnf = (2 << GPIO_CONF_SHIFT) | (1 << GPIO_MODE_SHIFT);
		break;
	case eGPIO_AF_OD:
		u8Cnf = (3 << GPIO_CONF_SHIFT) | (1 << GPIO_MODE_SHIFT);
		break;
	case eGPIO_IN_FLOAT:
		u8Cnf = (1 << GPIO_CONF_SHIFT) | (0 << GPIO_MODE_SHIFT);
		break;
	case eGPIO_IN_PU:
		u8Cnf = (2 << GPIO_CONF_SHIFT) | (0 << GPIO_MODE_SHIFT);
		break;
	case eGPIO_IN_PD:
		u8Cnf = (2 << GPIO_CONF_SHIFT) | (0 << GPIO_MODE_SHIFT);
		break;
	case eGPIO_IN_ANALOG:
		u8Cnf = (0 << GPIO_CONF_SHIFT) | (0 << GPIO_MODE_SHIFT);
		break;
	default:
		return;
		break;
	}

	/* Configure pin. Speed always will be 10MHz (Mode 0x01) */
	if(psGpioCfg->u8Pin <= 7)
	{
	    GPIO_CRL_CLR(psGpioCfg->ePort, psGpioCfg->u8Pin);
		GPIO_CRL_SET(psGpioCfg->ePort, psGpioCfg->u8Pin, u8Cnf);
	}
	else
	{
	    GPIO_CRH_CLR(psGpioCfg->ePort, psGpioCfg->u8Pin);
        GPIO_CRH_SET(psGpioCfg->ePort, psGpioCfg->u8Pin, u8Cnf);
	}



	// TODO: el alternate function va a ser solo para remapear algun periferico.
	// Si no se quiere remapear entonces basta con activar el pin como AF si es salida
	// o solo como entrada.

//	GPIO_OTYPER_SET(psGpioCfg->ePort, psGpioCfg->u8Pin, u8Cnf);
//	GPIO_OSPEEDR_SET(psGpioCfg->ePort, psGpioCfg->u8Pin, 2);
//
//	/* If alternate function was choosen */
//	if(u8Mode == 2)
//	{
//		if(psGpioCfg->u8Pin <= 7)
//			GPIO_AFRL_SET(psGpioCfg->ePort, psGpioCfg->u8Pin, psGpioCfg->u8AF);
//		else
//			GPIO_AFRH_SET(psGpioCfg->ePort, psGpioCfg->u8Pin, psGpioCfg->u8AF);
//	}
}

/*****************************************************************************************************
* \brief    Set a I/O pin
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnGpioSet(teGpioPort Port, u8 u8Pin)
{
	GPIO_BSRR_SET(Port, u8Pin);
}

/*****************************************************************************************************
* \brief    Get a I/O pin
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
u8 u8fnGpioGet(teGpioPort Port, u8 u8Pin)
{
	u16 u16Temp;

	u16Temp = GPIO_IDR_READ(Port);
	if( (u16Temp & (1 << u8Pin)) == (1 << u8Pin))
		return 1;
	else
		return 0;
}

/*****************************************************************************************************
* \brief    Reset a I/O pin
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnGpioReset(teGpioPort Port, u8 u8Pin)
{
	GPIO_BSRR_RESET(Port, u8Pin);
}

/*****************************************************************************************************
* \brief    Toggle a I/O pin
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnGpioToggle(teGpioPort Port, u8 u8Pin)
{
	u16 u16Temp;

	u16Temp = GPIO_IDR_READ(Port);
	if( (u16Temp & (1 << u8Pin)) == (1 << u8Pin))
		GPIO_BSRR_RESET(Port, u8Pin);
	else
		GPIO_BSRR_SET(Port, u8Pin);
}

/*****************************************************************************************************
* \brief    Write a I/O port
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnGpioWrite(teGpioPort Port, u16 u16Value)
{
	GPIO_ODR_WRITE(Port, u16Value);
}

/*****************************************************************************************************
* \brief    Read a I/O port
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
u16 vfnGpioRead(teGpioPort Port)
{
	return GPIO_IDR_READ(Port);
}
/***************************************End of Functions Definition**********************************/
