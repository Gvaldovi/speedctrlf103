/****************************************************************************************************/
/**
  \file         Gpio.h
  \brief        
  \author       Gerardo Valdovinos
  \project      Bluepill
  \version      
  \date         Mar 15, 2019

  THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/
#ifndef __GPIO_H
#define	__GPIO_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "Typedefs.h"
#include "GpioCfg.h"
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/
#define GPIO_ADDRESS_BASE					0x40010800
#define GPIO_ADDRESS_OFFSET					0x400

#define GPIO_CRL(p)							*(u32*)(GPIO_ADDRESS_BASE + (GPIO_ADDRESS_OFFSET * p) + 0x00)
#define GPIO_CRL_SET(Port, Pin, Cfg)		GPIO_CRL(Port) |= (u32)((Cfg & 0x0F) << (Pin * 4))
#define GPIO_CRL_CLR(Port, Pin)             GPIO_CRL(Port) &= ~((0x0F) << (Pin * 4))

#define GPIO_CRH(p)							*(u32*)(GPIO_ADDRESS_BASE + (GPIO_ADDRESS_OFFSET * p) + 0x04)
#define GPIO_CRH_SET(Port, Pin, Cfg)		GPIO_CRH(Port) |= (u32)((Cfg & 0x0F) << ((Pin-8) * 4))
#define GPIO_CRH_CLR(Port, Pin)             GPIO_CRH(Port) &= ~((0x0F) << ((Pin-8) * 4))

#define GPIO_IDR(p)							*(u32*)(GPIO_ADDRESS_BASE + (GPIO_ADDRESS_OFFSET * p) + 0x08)
#define GPIO_IDR_READ(Port)					(u16)GPIO_IDR(Port)

#define GPIO_ODR(p)							*(u32*)(GPIO_ADDRESS_BASE + (GPIO_ADDRESS_OFFSET * p) + 0x0C)
#define GPIO_ODR_WRITE(Port, v)				GPIO_ODR(Port) = (u32)v

#define GPIO_BSRR(p)						*(u32*)(GPIO_ADDRESS_BASE + (GPIO_ADDRESS_OFFSET * p) + 0x10)
#define GPIO_BSRR_SET(Port, Pin)			GPIO_BSRR(Port) |= (u32)((1 << Pin) & 0x0000FFFF)
#define GPIO_BSRR_RESET(Port, Pin)			GPIO_BSRR(Port) |= (u32)((1 << (Pin + 16)) & 0xFFFF0000)

//#define GPIO_AFRL(p)						*(u32*)(GPIO_ADDRESS_BASE + (GPIO_ADDRESS_OFFSET * p) + 0x20)
//#define GPIO_AFRL_SET(Port, Pin, Af)		GPIO_AFRL(Port) |= (u32)((Af & 0x0F) << (Pin * 4))
//
//#define GPIO_AFRH(p)						*(u32*)(GPIO_ADDRESS_BASE + (GPIO_ADDRESS_OFFSET * p) + 0x24)
//#define GPIO_AFRH_SET(Port, Pin, Af)		GPIO_AFRH(Port) |= (u32)((Af & 0x0F) << ((Pin - 8) * 4))

#define GPIO_AHB1ENR(x)						RCC->APB2ENR |= (u32)(1 << (x+2))
/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/

/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
void vfnGpioInit(const tsGpioDriverCfg *DriverCfg);

void vfnGpioCfg(tsGpioChannelCfg *GpioCfg);
void vfnGpioSet(teGpioPort Port, u8 u8Pin);
u8 u8fnGpioGet(teGpioPort Port, u8 u8Pin);
void vfnGpioReset(teGpioPort Port, u8 u8Pin);
void vfnGpioToggle(teGpioPort Port, u8 u8Pin);
void vfnGpioWrite(teGpioPort Port, u16 u16Value);
u16 vfnGpioRead(teGpioPort Port);

#endif	/* __GPIO_H */
/***************************************End of File**************************************************/
