/****************************************************************************************************/
/**
  \file         UartCfg.h
  \brief
  \author       Gerardo Valdovinos
  \project      Bluepill
  \version
  \date         Mar 15, 2019

  THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/
#ifndef __UART_CFG_H /*Duplicated Includes Prevention*/
#define __UART_CFG_H
/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "TypeDefs.h"
#include "Gpio.h"
/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/
/* This numbers for baud rate are from data sheet STM32F103 pag. 797.
 * This works with a mantisa and fraction.
 * USART1 is in PCLK2 (72MHz), others are in PCLK1 (36MHz) */
// f_PCLK = 36MHz
#define UART_P1_BAUD_9600			0x0EA6
#define UART_P1_BAUD_19200			0x0753
#define UART_P1_BAUD_57600			0x0271
#define UART_P1_BAUD_115200			0x0138
#define UART_P1_BAUD_230400			0x009C
// f_PCLK = 72MHz
#define UART_P2_BAUD_9600			0x1D4C
#define UART_P2_BAUD_19200			0x0EA6
#define UART_P2_BAUD_57600			0x04E2
#define UART_P2_BAUD_115200			0x0271
#define UART_P2_BAUD_230400			0x0138

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Uart Buffers~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* This buffer sizes defines are for not using FreeRTOS malloc in this module */
#define UART_CH1_BUFFER_SIZE		256
#define UART_CH2_BUFFER_SIZE		100
#define UART_CH3_BUFFER_SIZE		1
/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef enum
{
	eUART_CH1 = 0,
	eUART_CH2,
	eUART_CH3,
	eUART_CH_MAX
}teUartChannel;

typedef enum
{
	eUART_PARITY_EVEN = 0,
	eUART_PARITY_ODD,
	eUART_PARITY_NO,
}teUartParity;

typedef enum
{
	eUART_LENGTH_8 = 8,
	eUART_LENGTH_9
}teUartLength;

typedef struct
{
	teUartChannel       		eChannel;      		/* Uart channel */
	teUartLength                eWordLength;   		/* Word Lenght */
	teUartParity                eParity;	       	/* Parity */
	u16                 		u16Baud;        	/* Baud rate */

	u8*							pu8RxBuffer;		/* Pointer to Rx Buffer */
	IRQn_Type           		tIRQ;           	/* Interrupt request routine */

	tsGpioChannelCfg			sGpioChannelCfgTx;	/* Gpio Tx configuration */
	tsGpioChannelCfg			sGpioChannelCfgRx;	/* Gpio Rx configuration */
}tsUartChannelCfg;

typedef struct
{
	u8                          u8Channels; 		/* Number of channels */
	const tsUartChannelCfg      *psChannelCfg;  	/* Channels configuration pointer */
}tsUartDriverCfg;

/*****************************************************************************************************
*                                       Declaration of VARIABLEs                                     *
*****************************************************************************************************/
extern const tsUartDriverCfg sUartDriverCfg;
/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
#endif /* __UARTCFN_H */
/***************************************End of File**************************************************/
