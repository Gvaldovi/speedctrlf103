/****************************************************************************************************/
/**
  \file         iFlash.c
  \brief        
  \author       Gerardo Valdovinos
  \project      ScorpioARM1.0
  \version      0.0.1
  \date         Mar 1, 2016

  Program compiled with "COMPILER VERSION",
  Tested on "IK MASTER" board.

  ESTE DOCUMENTO Y SU CONTENIDO ES PROPIEDAD DE SIREMCO S.A.P.I. DE C.V. LAS COPIAS IMPRESAS DE ESTE
  DOCUMENTO SON COPIAS NO CONTROLADAS. PROHIBIDA SU REPRODUCCION PARCIAL O TOTAL SIN CONSENTIMIENTO
  DE SIREMCO S.A.P.I. DE C.V. SIREMCO(R) - CONFIDENCIAL -
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
#include "iFlash.h"

/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/

/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Flash unlock
* \author   Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfniFlashUnlock(void)
{
	/* Wait for flash ready */
	while ((FLASH->SR & FLASH_SR_BSY) != 0){}

	if ((FLASH->CR & FLASH_CR_LOCK) != 0)
	{
		FLASH->KEYR = FLASH_FKEY1;
		FLASH->KEYR = FLASH_FKEY2;
	}
}

/*****************************************************************************************************
* \brief    Flash Application erase. STM32F407 has 12 sectors of: 4 (16KB), 1 (64KB) and 7 (128KB). In
* 			the first sector (16KB) is the bootloader code.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfniFlashAppErase(void)
{
	u8 i;

	/* Unlock Flash memory */
	vfniFlashUnlock();

	/* Erase sectors from 1 to 12 */
	for(i = 1;i <= 12;i++)
	{
		/* Wait for flash ready */
		while ((FLASH->SR & FLASH_SR_BSY) != 0){}

		/* Select sector to erase */
		FLASH->CR |= FLASH_CR_SER | (i << 3);

		/* Start operation */
		FLASH->CR |= FLASH_CR_STRT;
	}

	/* Wait for flash ready */
	while ((FLASH->SR & FLASH_SR_BSY) != 0){}
}

/*****************************************************************************************************
* \brief    Flash programming
* \author   Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
u8 u8iFlashProgramming(u8 *pu8Address, u8 *pu8Data, u16 u16Size)
{
	u16 u16Index;

	__disable_irq();

	/* Unlock Flash memory */
	vfniFlashUnlock();

	/* Parallelism: Programing 32 bits every time */
	FLASH->CR |= FLASH_CR_PSIZE_1;

	/* Programm Flash page */
	FLASH->CR |= FLASH_CR_PG;
	for(u16Index = 0; u16Index < (u16Size/4); u16Index++)
	{
		/* Wait for flash ready */
		while ((FLASH->SR & FLASH_SR_BSY) != 0) {}

		/* Write 16bits of code */
		*(__IO uint32_t*)(pu8Address) = *(u32*)pu8Data;

		/* Increment by 2 the pointers */
		pu8Address += 4;
		pu8Data += 4;
	}
	FLASH->CR &= ~FLASH_CR_PG;

	/* Lock Flash memory */
	FLASH->CR |= FLASH_CR_LOCK;

	__enable_irq();

	return TRUE;
}
/***************************************End of Functions Definition**********************************/
