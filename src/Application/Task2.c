/****************************************************************************************************/
/**
  \file         IKMaster.c
  \brief
  \author       Gerardo Valdovinos
  \project      IKMaster
  \version      0.0.1
  \date         Aug 8, 2016
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
/* STM32 Library includes. */
#include <stdio.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"

/* Own includes */
#include "Task2.h"

/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                       Variables EXTERN                                             *
*****************************************************************************************************/
/* External RTOS objects */
extern TaskHandle_t xTask1T;
extern TaskHandle_t xTask3T;
extern QueueHandle_t xQueue1Q;
extern QueueHandle_t xQueue3Q;
/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/
/* Internal RTOS objects */
TaskHandle_t xTask2T = NULL;
QueueHandle_t xQueue2Q = NULL;
/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/
//static void vfnTask2Queue(tsMsg *psMsg);
/*****************************************************************************************************
*                                    Definition of FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief	Init function.
* \author   Gerardo Valdovinos
* \param    void
* \return   int
* \notes
*****************************************************************************************************/
void vfnTask2Init(void)
{
	xTaskCreate( vfnTask2, "Task2", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 3, xTask2T );
	xQueue2Q = xQueueCreate(5, sizeof(tsMsgQ*));
}

/*****************************************************************************************************
* \brief	Main function. Create first tasks, queues and initialize every module.
* \author   Gerardo Valdovinos
* \param    void
* \return   int
*****************************************************************************************************/
void vfnTask2( void *pvParameters )
{
	tsMsgQ *psTask2MsgRx;

	for( ;; )
	{
		if(pdTRUE == xQueueReceive(xQueue2Q, &psTask2MsgRx, portMAX_DELAY))
		{
			;
		}
	}
}

/*****************************************************************************************************
* \brief	Commands by Queue.
* \author   Gerardo Valdovinos
* \param    void
* \return   int
*****************************************************************************************************/
//static void vfnTask2Queue(tsMsg *psMsg)
//{
//	return;
//}
