/****************************************************************************************************/
/**
  \file         IKMaster.c
  \brief
  \author       Gerardo Valdovinos
  \project      IKMaster
  \version      0.0.1
  \date         Aug 8, 2016
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
/* STM32 Library includes. */
#include <stdio.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"

/* Own includes */
#include "Utility.h"
#include "iRtc.h"
#include "Gpio.h"
#include "Uart.h"
//#include "Task1.h"
//#include "Task2.h"
//#include "Task3.h"

/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                       Variables EXTERN                                             *
*****************************************************************************************************/
/* External RTOS objects */
//extern TaskHandle_t xTask1T;
//extern TaskHandle_t xTask2T;
//extern TaskHandle_t xTask3T;
//extern QueueHandle_t xQueue1Q;
//extern QueueHandle_t xQueue2Q;
//extern QueueHandle_t xQueue3Q;
/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/
/* RTOS objects */
TimerHandle_t xTimer1T = NULL;
/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/
static void svfnInit(void);
static void svfnInitRtosObj(void);

/* TEST FUNCTIONS */
static void svfnUart1RxComplete(void);

/* Static functions for RTOS objects */
static void svfnTimer1T(TimerHandle_t xTimer);
/*****************************************************************************************************
*                                    Definition of FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief	Main function. Create first tasks, queues and initialize every module.
* \author   Gerardo Valdovinos
* \param    void
* \return   int
* \notes 	For STM32F103C8:
* 			Crystal = 8MHz, f(PLL) = 72MHz
* 			SYSCLK = 72MHz, AHB = 72MHz, PCLK1(APB1) = 36MHz, PCLK2(APB2) = 72MHZ
*****************************************************************************************************/
//Notas
// vTaskGetInfo() para obtener status (TaskStatus_t) de la tarea incluyendo estado, stack disponible, apuntador a funcion base, tiempo runtime
// uxTaskGetSystemState(). llena la estructura TaskStatus_t para todas las tareas
int main(void)
{
	/* Configure priority groups */
	NVIC_PriorityGroupConfig( NVIC_PriorityGroup_4 );

	/* Initialize the system */
	svfnInit();

	/* Initialize RTOS objects */
	svfnInitRtosObj();

	taskENABLE_INTERRUPTS();

	/* Start the tasks and timer running. */
	vTaskStartScheduler();


	/* If all is well, the scheduler will now be running, and the following line
	will never be reached.  If the following line does execute, then there was
	insufficient FreeRTOS heap memory available for the idle and/or timer tasks
	to be created.  See the memory management section on the FreeRTOS web site
	for more details. */
	for( ;; );
}

/*****************************************************************************************************
* \brief	Initialization of Low Level Drivers.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
static void svfnInit(void)
{
	/* Init RTC */
//	vfniRtcInit();

	/* Init GPIO */
	vfnGpioInit(&sGpioDriverCfg);

	/* Init Uart */
	vfnUartInit(&sUartDriverCfg);
    /* Initialize Uart interface */
    vfnUartSetCallbacks(eUART_CH1, NULL, svfnUart1RxComplete);
    vfnUartPower(eUART_CH1, eUART_ON);
}

/*****************************************************************************************************
* \brief	Initialization of RTOS objects.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
static void svfnInitRtosObj(void)
{
	xTimer1T = xTimerCreate("Timer1", (10000 / portTICK_PERIOD_MS ), pdTRUE, ( void * ) 0, svfnTimer1T);
	xTimerStart(xTimer1T, 0);
}

/*****************************************************************************************************
* \brief	Initialization of RTOS objects.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
static void svfnTimer1T(TimerHandle_t xTimer)
{
    static u8 i = 0;
	vfnGpioToggle(eGPIO_PORTC, 13);

	/* TEST: Send one byte */
	i++;
	u8fnUartWrite(eUART_CH1, &i, 1);
}



/*****************************************************************************************************
*                                           TEST FUNCTIONS                                           *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Uart1 receive callback. TEST FUNCTION.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
static void svfnUart1RxComplete(void)
{
    vfnGpioToggle(eGPIO_PORTB, 7);
}



/*****************************************************************************************************
*                                        RTOS HOOK FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief	Functions related to FreeRTOS
* \author   FreeRTOS
* \param    void
* \return   void
*****************************************************************************************************/
void vApplicationMallocFailedHook( void )
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
	( void ) pcTaskName;
	( void ) pxTask;

	/* Run time stack overflow checking is performed if
	configconfigCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
	function is called if a stack overflow is detected. */
	for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
	volatile size_t xFreeStackSpace;

	/* This function is called on each cycle of the idle task.  In this case it
	does nothing useful, other than report the amout of FreeRTOS heap that
	remains unallocated. */
	xFreeStackSpace = xPortGetFreeHeapSize();

	if( xFreeStackSpace > 100 )
	{
		/* By now, the kernel has allocated everything it is going to, so
		if there is a lot of heap remaining unallocated then
		the value of configTOTAL_HEAP_SIZE in FreeRTOSConfig.h can be
		reduced accordingly. */
	}
}
/***************************************End of Functions Definition**********************************/
