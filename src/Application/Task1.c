/****************************************************************************************************/
/**
  \file         IKMaster.c
  \brief
  \author       Gerardo Valdovinos
  \project      IKMaster
  \version      0.0.1
  \date         Aug 8, 2016
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
/* STM32 Library includes. */
#include <stdio.h>

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"

/* Own includes */
#include "Utility.h"
#include "Task1.h"

/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                       Variables EXTERN                                             *
*****************************************************************************************************/
/* External RTOS objects */
//extern TaskHandle_t xTask2T;
//extern TaskHandle_t xTask3T;
//extern QueueHandle_t xQueue2Q;
//extern QueueHandle_t xQueue3Q;
/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/
/* Internal RTOS objects */
TaskHandle_t xTask1T = NULL;
QueueHandle_t xQueue1Q = NULL;

/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/
static teIntStatus efnTask1Msg(tsMsg *psMsg);
static teIntStatus efnTask1Ext(tsMsg *psMsg);
static teIntStatus efnTask1Int(tsMsg *psMsg);
/*****************************************************************************************************
*                                    Definition of FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief	Init function.
* \author   Gerardo Valdovinos
* \param    void
* \return   int
* \notes
*****************************************************************************************************/
void vfnTask1Init(void)
{
	xTaskCreate( vfnTask1, "Task1", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 3, xTask1T );
	xQueue1Q = xQueueCreate(5, sizeof(tsMsg));
}

/*****************************************************************************************************
* \brief	Main function. Create first tasks, queues and initialize every module.
* \author   Gerardo Valdovinos
* \param    void
* \return   int
*****************************************************************************************************/
void vfnTask1( void *pvParameters )
{
	tsMsg sMsg;
	teIntStatus eIntStatus;

	for( ;; )
	{
		if(pdTRUE == xQueueReceive(xQueue1Q, &sMsg, pdMS_TO_TICKS(100)))
		{
			/* Message arrived */
			eIntStatus = efnTask1Msg(&sMsg);

			/* Response */
			if(eIntStatus == eINT_STATUS_OK)
				efnUtilitySend(&sMsg, xQueue1Q, 100, 1);
			else
				; // guarda error interno
		}
	}
}

/*****************************************************************************************************
* \brief	Commands by Queue.
* \author   Gerardo Valdovinos
* \param    void
* \return   int
*****************************************************************************************************/
static teIntStatus efnTask1Msg(tsMsg *psMsg)
{
	u8 au8Rx[100];
	tsMsg sMsg;
	teIntStatus eIntStatus;

	/* Copy to local buffer or maintain a pointer to data (master buffer). */
	if(psMsg->sInfo.eFlag != eINT_FLAG_MASTER_BUFFER)
	{
		psMsg->pu8Data = &au8Rx[0];
		memcpy(&au8Rx[0], psMsg->pu8Data, psMsg->u16Size);
	}

	/* Warn the origin task that message arrived correctly. */
	sMsg.sInfo.eTaskDest = psMsg->sInfo.eTaskOrig;
	sMsg.sInfo.eTaskOrig = eTASK1;
	sMsg.sInfo.eFlag = eINT_FLAG_EXECUTE_OK;
	sMsg.sInfo.eIntCmd = eINT_CMD_NONE;
	eIntStatus = efnUtilitySend(&sMsg, xQueue1Q, 100, 2);

	if(eIntStatus == eINT_STATUS_OK)
	{
		/* Execute command */
		if(sMsg.sInfo.eIntCmd == eINT_CMD_EXT)
			eIntStatus = efnTask1Ext(&sMsg);
		else
			eIntStatus = efnTask1Int(&sMsg);
	}

	return eIntStatus;
}

/*****************************************************************************************************
* \brief	Commands execution.
* \author   Gerardo Valdovinos
* \param    void
* \return   int
*****************************************************************************************************/
static teIntStatus efnTask1Ext(tsMsg *psMsg)
{
	// TODO: cuando se integre comunicacion hacia perifericos (uart principlamente) la funcion de
	// comunicacion debe enviar cuanto tiempo va a esperar
	// si solo va a recibir datos o enviar/recibir datos (gsm modems usan muchas veces doble respuesta)

	/* Inter-task communication */
	tsMsg sMsg;
	u8 au8Local[100];
	teIntStatus eIntStatus = eINT_STATUS_UNDEFINED;

	/* External communication */
	tsHeader sHeader;

	/* Copy and parse data */
	memcpy((u8*)&sHeader, &psMsg->pu8Data[0], sizeof(tsHeader));

	switch(sHeader.u8Cmd)
	{
	case 0:
		// Comando: escribir datos
		memcpy(&au8Local[0], &psMsg->pu8Data[sizeof(tsHeader)], sHeader.u16Size);


		// Construir respuesta externa sin datos, estatus OK
		eIntStatus = eINT_STATUS_OK;
		psMsg->u16Size = u16fnUtilityCmd(&sHeader, eSTATUS_OK, 0, psMsg->pu8Data);
		// Construir respuesta interna
		vfnUtilityMsg(psMsg, eINT_FLAG_NONE, eINT_CMD_NONE);
		break;
	case 1:
		// Comando: escribir datos
		memcpy(&au8Local[0], &psMsg->pu8Data[sizeof(tsHeader)], sHeader.u16Size);


		// Envia: noti a otra tarea y espera termino
			// Mensaje
		sMsg.sInfo.eTaskDest = eTASK2;
		sMsg.sInfo.eTaskOrig = eTASK1;
		sMsg.sInfo.eFlag = eINT_FLAG_EXECUTE;
		sMsg.sInfo.eIntCmd = eINT_CMD_INT_1;
			// Comunicacion
		eIntStatus = efnUtilitySend(&sMsg, xQueue1Q, 500, 3);


		// Construir respuesta externa sin datos
		if(eIntStatus == eINT_STATUS_OK)
			psMsg->u16Size = u16fnUtilityCmd(&sHeader, eSTATUS_OK, 0, psMsg->pu8Data);
		else
			psMsg->u16Size = u16fnUtilityCmd(&sHeader, eSTATUS_NOK, 0, psMsg->pu8Data);


		// Construir respuesta interna
		vfnUtilityMsg(psMsg, eINT_FLAG_NONE, eINT_CMD_NONE);
		break;
	case 2:
		// Envia: datos a otra tarea y espera termino
		memcpy(&au8Local[0], "hola", sizeof("hola"));
		sMsg.sInfo.eTaskDest = eTASK3;
		sMsg.sInfo.eTaskOrig = eTASK1;
		sMsg.sInfo.eFlag = eINT_FLAG_EXECUTE;
		sMsg.sInfo.eIntCmd = eINT_CMD_INT_2;
		sMsg.pu8Data = &au8Local[0];
		sMsg.u16Size = sizeof("hola");
			// Comunicacion
		efnUtilitySend(&sMsg, xQueue1Q, 500, 3);



		// Envia: datos a otra tarea y espera datos
		memcpy(&au8Local[0], "mundo", sizeof("mundo"));
		sMsg.sInfo.eTaskDest = eTASK3;
		sMsg.sInfo.eTaskOrig = eTASK1;
		sMsg.sInfo.eFlag = eINT_FLAG_DATA;
		sMsg.sInfo.eIntCmd = eINT_CMD_INT_3;
		sMsg.pu8Data = &au8Local[0];
		sMsg.u16Size = sizeof("mundo");
			// Comunicacion
		eIntStatus = efnUtilitySend(&sMsg, xQueue1Q, 500, 3);

		// Construir respuesta externa con datos
		if(eIntStatus == eINT_STATUS_OK)
		{
			memcpy(&psMsg->pu8Data[0], &au8Local[0], sMsg.u16Size);
			psMsg->u16Size = u16fnUtilityCmd(&sHeader, eSTATUS_OK, sMsg.u16Size, psMsg->pu8Data);
		}
		else
			psMsg->u16Size = u16fnUtilityCmd(&sHeader, eSTATUS_NOK, 0, psMsg->pu8Data);

		// Construir respuesta interna
		vfnUtilityMsg(psMsg, eINT_FLAG_NONE, eINT_CMD_NONE);
		break;
	default:
		break;
	}

	return eIntStatus;
}

/*****************************************************************************************************
* \brief	Internal Commands execution.
* \author   Gerardo Valdovinos
* \param    void
* \return   int
*****************************************************************************************************/
static teIntStatus efnTask1Int(tsMsg *psMsg)
{
	// TODO: cuando se integre comunicacion hacia perifericos (uart principlamente) la funcion de
	// comunicacion debe enviar cuanto tiempo va a esperar
	// si solo va a recibir datos o enviar/recibir datos (gsm modems usan muchas veces doble respuesta)

	/* Inter-task communication */
//	tsMsg sMsg;
//	u8 au8Local[100];
	teIntStatus eIntStatus = eINT_STATUS_UNDEFINED;

	switch(psMsg->sInfo.eIntCmd)
	{
	case eINT_CMD_INT_1:
		break;
	case eINT_CMD_INT_2:
		break;
	case eINT_CMD_INT_3:
		break;
	default:
		break;
	}

	return eIntStatus;
}

/*****************************************************************************************************
* \brief	Task1 send queues.
* \author   Gerardo Valdovinos
* \param    void
* \return   int
*****************************************************************************************************/
//static teStatus efnTask1Send(tsMsg *psMsg, u32 u32Timeout, u8 u8ErrorCount)
//{
//	teIntStatus eIntStatus = eINT_STATUS_UNDEFINED;
//	tsMsg sMsg;
//	u8 i;
//
//	/* Verify NULL pointer */
//	if((psMsg == NULL) || (psMsg->pu8Data == NULL))
//		return eINT_STATUS_ERROR_DATA_POINTER;
//
//	/* Verify flags */
//	if( (psMsg->sInfo.eFlag != eINT_FLAG_ACTION)	&&
//		(psMsg->sInfo.eFlag != eINT_FLAG_EXECUTE)	&&
//		(psMsg->sInfo.eFlag != eINT_FLAG_DATA))
//		return eINT_STATUS_ERROR_ACTION;
//
//	/* Try u8ErrorCount times */
//	for(i = 0;i < u8ErrorCount;i++)
//	{
//		// TODO: validar si efectivamente se envio el mensaje a la cola
//		if(psMsg->sInfo.eTaskDest == eTASK2)
//			xQueueSend(xQueue2Q, psMsg, 0);
//		if(psMsg->sInfo.eTaskDest == eTASK3)
//			xQueueSend(xQueue3Q, psMsg, 0);
//
//		if(psMsg->sInfo.eFlag == eINT_FLAG_ACTION)
//		{
//			eIntStatus = eINT_STATUS_OK;
//			break;
//		}
//		else
//		{
//			if(pdTRUE == xQueueReceive(xQueue1Q, &sMsg, pdMS_TO_TICKS(u32Timeout)))
//			{
//				if(sMsg.sInfo.eTaskDest == psMsg->sInfo.eTaskOrig)
//				{
//					// TODO: agregar que cuando haya un execute y la respuesta sea data entonces reencolar
//					if(psMsg->sInfo.eFlag == eINT_FLAG_EXECUTE)
//					{
//						if(sMsg.sInfo.eFlag == eINT_FLAG_EXECUTE_OK)
//							eIntStatus = eINT_STATUS_OK;
//						else if(sMsg.sInfo.eFlag == eINT_FLAG_EXECUTE_NOK)
//							eIntStatus = eINT_STATUS_ERROR_EXECUTE;
//					}
//					else if(psMsg->sInfo.eFlag == eINT_FLAG_DATA)
//					{
//						if(sMsg.sInfo.eFlag == eINT_FLAG_DATA_OK)
//						{
//							memcpy(&psMsg->pu8Data[0], &sMsg.pu8Data[0], sMsg.u16Size);
//							psMsg->u16Size = sMsg.u16Size;
//							eIntStatus = eINT_STATUS_OK;
//						}
//						else if(sMsg.sInfo.eFlag == eINT_FLAG_DATA_NOK)
//							eIntStatus = eINT_STATUS_ERROR_DATA;
//					}
//					else
//						eIntStatus = eINT_STATUS_ERROR_ALLOWED_ACTION;
//					break;
//				}
//				else
//				{
//					/* Unexpected item, send it to queue's back */
//					xQueueSendToBack(xQueue1Q, sMsg, 0);
//					eIntStatus = eINT_STATUS_ERROR_BAD_TASK;
//				}
//			}
//			else
//				eIntStatus = eINT_STATUS_ERROR_TIMEOUT;
//		}
//	}
//
//	return eIntStatus;
//}

/*****************************************************************************************************
* \brief	Task1 send Notifications.
* \author   Gerardo Valdovinos
* \param    void
* \return   int
*****************************************************************************************************/
//static teStatus efnTask1SendN(tsMsgN *psMsgN, u32 u32Timeout, u8 u8Count)
//{
//	teStatus eStatus;
//	u32 u32Noti;
//	tsMsgN *psMsg = (tsMsgN*)&u32Noti;
//	TaskHandle_t xTask;
//	u8 i;
//
//	/* Verify NULL pointer */
//	if(psMsgN != NULL)
//		return eSTATUS_ERROR_POINTER;
//
//	/* Verify flags */
//	if( (psMsgN->sInfo.eFlag != eFLAG_WARN_WHEN_FINISH) ||
//		(psMsgN->sInfo.eFlag != eFLAG_ACTION))
//		return eSTATUS_ERROR_ACTION;
//
//	/* Try u8ErrorCount times */
//	for(i = 0;i < u8Count;i++)
//	{
//		xTask = xfnUtilityTasks(psMsgN->sInfo.eTaskDest);
//		xTaskNotify(xTask, *psMsgN, eSetValueWithOverwrite);
//
//		if(psMsgN->sInfo.eFlag == eFLAG_ACTION)
//		{
//			eStatus = eSTATUS_OK;
//			break;
//		}
//		else
//		{
//			u32Noti = ulTaskNotifyTake( pdTRUE, pdMS_TO_TICKS(u32Timeout));
//			if(psMsg->sInfo.eFlag != 0)
//			{
//				if(psMsg->sInfo.eTaskDest == psMsgN->sInfo.eTaskOrig)
//				{
//					if((psMsg->sInfo.eFlag == eFLAG_EXECUTED) && (psMsgN->sInfo.eFlag == eFLAG_WARN_WHEN_FINISH))
//						eStatus = eSTATUS_OK;
//					else
//						eStatus = eSTATUS_ERROR_ALLOWED_ACTION;
//					break;
//				}
//				else
//				{
//					// TODO: Meterla a un buffer circular
//					eStatus = eSTATUS_ERROR_BAD_TASK;
//				}
//			}
//			else
//				eStatus = eSTATUS_ERROR_TIMEOUT;
//		}
//	}
//
//	return eStatus;
//}

/*****************************************************************************************************
* \brief	Commands by Notification.
* \author   Gerardo Valdovinos
* \param    void
* \return   int
*****************************************************************************************************/
//static void vfnTask1Noti(tsNoti sNoti)
//{
//	switch(sNoti.eIntCmd)
//	{
//	case 0:
//		// Execute command
//
//
//		if(sNoti.sInfo.eFlag == eFLAG2) // Response?
//		{
//			xTaskNotify(xfnUtilityTasks(sNoti.eTask), eNoti, eSetValueWithOverwrite);
//		}
//		break;
//	case 1:
//		break;
//	default:
//		break;
//	}
//}

/***************************************End of Functions Definition**********************************/
